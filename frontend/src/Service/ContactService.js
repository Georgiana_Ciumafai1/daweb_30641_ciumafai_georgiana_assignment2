import axios from "axios";

export class ContactService {
  static root = "http://localhost:5000";

  static sendMessage(formModel) {
    return new Promise((resolve, reject) => {
      axios.post(this.root + "/contact", formModel).then(
        (response) => {
          resolve(response);
        },
        (error) => {
          reject(error.respone);
          console.log("iar");
        }
      );
    });
  }

  static validateForm(formModel, errors) {
    let isValid = true;
    let expr1 = /^[0-9a-zA-z_-]+$/;

    if (formModel.name === "") {
      errors.name = "Fill in your name";
      isValid = false;
    } else {
      errors.name = "";
    }

    expr1 = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (formModel.email === "" || formModel.email.match(expr1) === null) {
      errors.email = "E-mail not valid";
      isValid = false;
    } else {
      errors.email = "";
    }
    return isValid;
  }
}
