import * as React from "react";
import "../../src/css/Header.css";
import { useTranslation } from "react-i18next";
function MyComponent() {
  const { t } = useTranslation();
  return <strong>{t("header.1")}</strong>;
}
function MyComponent2() {
  const { t } = useTranslation();
  return <strong>{t("header.2")}</strong>;
}
export class Header extends React.Component {
  render() {
    return (
      <div className="header-style">
        <header>
          <MyComponent />
        </header>
        <header>
          <MyComponent2 />
        </header>
      </div>
    );
  }
}
