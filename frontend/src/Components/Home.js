import * as React from "react";
import signup from "../img/download.png";
import signup2 from "../img/py.png";
import signup3 from "../img/CLIPS.png";
import "../css/Home.css";
import { useTranslation } from "react-i18next";
function fetchHelloWorld() {
  console.log("fetching python localhost");
  fetch("http://localhost:5000/", {
    method: "GET",
    mode: "no-cors",
    dataType: "json",
  })
    .then((r) => r.json())
    .then((r) => {
      console.log(r);
    })
    .catch((err) => console.log(err));
}
function Home() {
  const { t } = useTranslation();

  return (
    
    <div className="banner-text">
      <br></br>
      <p> {t("home.1")}</p>
      <p> {t("home.2")}</p>
      <img src={signup} alt="signup" className="avatar-img" />
      <hr></hr>
      <img src={signup2} alt="signup2" className="avatar-img" />
      <img src={signup3} alt="signup3" className="avatar-img" />
      <p></p>
      <div></div>
    </div>
  );
}

export default Home;
