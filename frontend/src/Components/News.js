import React, { Suspense } from "react";
import YouTube from "react-youtube";
import { useTranslation } from "react-i18next";
import XMLParser from "react-xml-parser";

function News() {
  var pathxml = process.env.PUBLIC_URL + "news.xml";

  // var XMLParser = require("react-xml-parser");
  // var xml = new XMLParser().parseFromString(xmlText);
  //var jsonDataFromXml = new XMLParser().parseFromString(xmlData);
  //console.log(jsonDataFromXml);
  const { t } = useTranslation();
  var parseString = require("react-native-xml2js").parseString;
  // var xml = "<news><nw>news.1</nw><nw>news.2</nw><nw>news.3</nw></news>";
  //const parseString = require("react-native-xml2js").parseString;
  console.log(pathxml); //asa am aflat cum se numeste fisierul meu xml pe care se face fetch

  fetch(pathxml)
    .then((response) => response.text())
    .then((response) => {
      parseString(response, function (err, result) {
        //console.log(obj.nw[0]);
        console.log(result);
        console.log(result.news.nw[0]);
        console.log(result.news.nw[1]);
        console.log(result.news.nw[2]);
        localStorage.setItem("nw1", result.news.nw[0].trim());
        localStorage.setItem("nw2", result.news.nw[1].trim());
        localStorage.setItem("nw3", result.news.nw[2].trim());
        // console.log(xml.getElementsByTagName("news").childNodes[0]);
        //localStorage.setItem("nw1", xml[0]);
        //localStorage.setItem("nw2", xml[1]);
        // localStorage.setItem("nw3", xml[2]);
      });
    })
    .catch((err) => {
      console.log("fetch", err);
    });

  return (
    <>
      <div className="banner-text">
        <p>{t(localStorage.getItem("nw1"))}</p>;
        <p>{t(localStorage.getItem("nw2"))}</p>;
        <p>{t(localStorage.getItem("nw3"))}</p>;
      </div>
      <div> </div>
      <br></br>
      <YouTube videoId="KgPSREMmA_0" />
      <br></br>
      <br></br>
    </>
  );
}

export default News;
