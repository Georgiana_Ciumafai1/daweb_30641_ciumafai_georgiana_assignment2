import * as React from "react";
import "../css/About.css";
import { Grid, Cell } from "react-mdl";
import { useTranslation } from "react-i18next";

function About() {
  const { t } = useTranslation();
  return (
    <div style={{ width: "100%", margin: "auto" }}>
      <Grid className="landing-grid">
        <Cell col={2}>
          <div className="banner-text">
            <p>
              <h1>{t("about.1")}</h1>
            </p>
          </div>
        </Cell>
        <Cell col={2}>
          <div className="banner-text">
            <p>
              <hr />
              <h1> {t("about.2")} </h1>

              <hr />
            </p>

            <p>{t("about.3")}</p>
          </div>
        </Cell>
      </Grid>
    </div>
  );
}
export default About;
